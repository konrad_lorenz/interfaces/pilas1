import java.util.Stack;

public class Main {
    public static void main(String[] args) {

        Stack<Integer> pila =  new Stack<>();

        pila.push(1);
        pila.push(2);
        pila.push(3);

        System.out.println("Esta es la pila inicial: " +  pila);

        int elementoEnParteSuperior = pila.peek();
        System.out.println("Elemento parte superior: " +  elementoEnParteSuperior);

        int elementoAEliminar = pila.pop();
        System.out.println("Elemento parte superior eliminado: " +  elementoAEliminar);

        System.out.println("Esta es la pila despues de borrar: " +  pila);

        boolean estaVacia = pila.empty();
        System.out.println("Esta vacia?: " +  estaVacia);


        int tamayo = pila.size();
        System.out.println("Tamayo pila: " +  tamayo);







    }
}